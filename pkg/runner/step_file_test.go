package runner_test

import (
	"bytes"
	"io"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"google.golang.org/protobuf/encoding/protojson"

	"gitlab.com/gitlab-org/step-runner/pkg/runner"
	"gitlab.com/gitlab-org/step-runner/pkg/testutil/bldr"
	"gitlab.com/gitlab-org/step-runner/proto"
)

func TestStepFile(t *testing.T) {
	t.Run("step file is created empty", func(t *testing.T) {
		stepFile, err := runner.NewStepFileInDir(t.TempDir())
		require.NoError(t, err)

		file, err := os.Open(stepFile.Path())
		require.NoError(t, err)

		contents, err := io.ReadAll(file)
		require.NoError(t, err)
		require.Len(t, contents, 0)
	})

	t.Run("read as step result", func(t *testing.T) {
		stepFile, err := runner.NewStepFileInDir(t.TempDir())
		require.NoError(t, err)

		file, err := os.OpenFile(stepFile.Path(), os.O_WRONLY, 0666)
		require.NoError(t, err)
		defer func() { _ = file.Close() }()

		stepResult := bldr.StepResult().WithFailedStatus().Build()
		data, err := protojson.Marshal(stepResult)
		require.NoError(t, err)

		_, err = io.Copy(file, bytes.NewReader(data))
		require.NoError(t, err)

		loadedStepResult, err := stepFile.ReadStepResult()
		require.NoError(t, err)
		require.Equal(t, proto.StepResult_failure, loadedStepResult.Status)
	})
}

func TestStepFile_ReadAsDotEnv(t *testing.T) {
	tests := map[string]struct {
		data    string
		want    map[string]string
		wantErr string
	}{
		"standard key/value": {
			data: `NAME=VALUE`,
			want: map[string]string{"NAME": "VALUE"},
		},
		"lowercase key/value": {
			data: `name=value`,
			want: map[string]string{"name": "value"},
		},
		"comments are stripped from lines": {
			data: `PROJECT TITLE=My Project # is this a comment`,
			want: map[string]string{"PROJECT TITLE": "My Project"},
		},
		"new lines can be added to value surrounded by quotes": {
			data: `KEY="one
two"`,
			want: map[string]string{"KEY": "one\ntwo"},
		},
		"new lines can be added to value surrounded by single quotes": {
			data: `KEY='one
two'`,
			want: map[string]string{"KEY": "one\ntwo"},
		},
		"unicode cannot be used in key": {
			data:    `spaß=German for fun`,
			wantErr: `unexpected character "\u009f" in variable name near "spaß=German for fun`,
		},
		"unicode can be used in value": {
			data: `FUN=spaß`,
			want: map[string]string{"FUN": "spaß"},
		},
		"keys can start with a number": {
			data: `2_MUCH_FUN=always`,
			want: map[string]string{"2_MUCH_FUN": "always"},
		},
		"empty space is removed": {
			data: `NAME=VALUE

NAME2=VALUE2

`,
			want: map[string]string{"NAME": "VALUE", "NAME2": "VALUE2"},
		},
		"keys and values are trimmed for space": {
			data: ` NAME =     VALUE      `,
			want: map[string]string{"NAME": "VALUE"},
		},
		"quotes can be added to value by surrounding with single quotes": {
			data: `KEY='"VALUE"'`,
			want: map[string]string{"KEY": `"VALUE"`},
		},
		"expressions can be added as the value": {
			data: `NAME=${{inputs.name}}`,
			want: map[string]string{"NAME": `${{inputs.name}}`},
		},
		"expressions cannot be added as the key": {
			data:    `${{inputs.name}}=Name value`,
			wantErr: `unexpected character "$" in variable name near "${{inputs.name}}=Name value"`,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			stepFile, err := runner.NewStepFileInDir(t.TempDir())
			require.NoError(t, err)

			file, err := os.OpenFile(stepFile.Path(), os.O_WRONLY, 0666)
			require.NoError(t, err)
			defer func() { _ = file.Close() }()

			_, err = io.Copy(file, strings.NewReader(test.data))
			require.NoError(t, err)

			env, err := stepFile.ReadDotEnv()

			if test.wantErr == "" {
				require.NoError(t, err)
				require.Equal(t, test.want, env)
			} else {
				require.Error(t, err)
				require.Contains(t, err.Error(), test.wantErr)
			}
		})
	}
}

func TestStepFile_ReadAsKeyValueLines(t *testing.T) {
	tests := map[string]struct {
		data    string
		want    map[string]string
		wantErr string
	}{
		"standard key/value": {
			data: `name=value`,
			want: map[string]string{"name": "value"},
		},
		"more than one equals": {
			data: `name=foo=bar`,
			want: map[string]string{"name": "foo=bar"},
		},
		"JSON string": {
			data: `message="hello"`,
			want: map[string]string{"message": `"hello"`},
		},
		"unicode can be used in key": {
			data: `spaß=German for fun`,
			want: map[string]string{"spaß": "German for fun"},
		},
		"unicode can be used in value": {
			data: `FUN=spaß`,
			want: map[string]string{"FUN": "spaß"},
		},
		"empty space is removed": {
			data: `name=value

name2=value2

`,
			want: map[string]string{"name": "value", "name2": "value2"},
		},
		"keys and values are not trimmed for space": {
			data: ` name =     value      `,
			want: map[string]string{" name ": "     value      "},
		},
		"expressions can be added as the value": {
			data: `name=${{inputs.name}}`,
			want: map[string]string{"name": `${{inputs.name}}`},
		},
		"expressions can be added as the key": {
			data: `${{inputs.name}}=Name value`,
			want: map[string]string{`${{inputs.name}}`: "Name value"},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			stepFile, err := runner.NewStepFileInDir(t.TempDir())
			require.NoError(t, err)

			file, err := os.OpenFile(stepFile.Path(), os.O_WRONLY, 0666)
			require.NoError(t, err)
			defer func() { _ = file.Close() }()

			_, err = io.Copy(file, strings.NewReader(test.data))
			require.NoError(t, err)

			env, err := stepFile.ReadKeyValueLines()

			if test.wantErr == "" {
				require.NoError(t, err)
				require.Equal(t, test.want, env)
			} else {
				require.Error(t, err)
				require.Contains(t, err.Error(), test.wantErr)
			}
		})
	}
}
