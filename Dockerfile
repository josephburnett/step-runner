FROM golang:1.22

WORKDIR /app
COPY ./out/bin/step-runner-linux-amd64 /step-runner
RUN ln /step-runner /usr/bin/step-runner

# This is necessary only during the transition period while step-runner
# is distributed as an image and not built into the CI environment.
#
# https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
#
RUN apt-get update
RUN apt-get install -y ca-certificates curl
RUN install -m 0755 -d /etc/apt/keyrings
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
RUN chmod a+r /etc/apt/keyrings/docker.asc
RUN echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
      bookworm stable" | \
      tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt-get update
RUN apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

CMD ["/step-runner"]
